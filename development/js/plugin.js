(function( $ ) {
 
    $.fn.lightbox = function(options) {

        var defaults = {   
            debug:      false,         
            item:       'img',
            overlay:    '.m-lightbox-overlay',
            lightbox:   '.m-lightbox',
            caption:    '.m-lightbox-caption', 
            close:      '.m-lightbox-close',
            prev:       '.m-lightbox-prev',
            next:       '.m-lightbox-next'
        };
     
        var settings = $.extend( {}, defaults, options );

        var gallery = this;

        var render = function(obj, index, length) {
            //get image dimesions
            var tempImageObj = new Image();

            tempImageObj.onload = function() {
                var height = tempImageObj.height;
                var width = tempImageObj.width;

                width = (width > $(window).width()) ? ($(window).width() - 30) : width;
                height = (height > $(window).height()) ? ($(window).height() - 30) : height;

                $(settings.lightbox).css({
                    'width':        width + 'px',
                    'height':       height + 'px',
                    'margin-left':  '-' + (width / 2) + 'px',
                    'margin-top':   '-' + (height / 2) + 'px'
                });
            }
            tempImageObj.src = obj.data('src');

            //render lightbox
            $(settings.lightbox).css({
                'background-image': 'url("' + obj.data('src')  + '")'
            })
            .attr('data-gallery', obj.data('gallery'))
            .attr('data-index', obj.data('index'))
            .find(settings.caption).html(
                '<span class="count">Image ' + index + ' of ' + length + '</span>' +
                '<span class="text">' + obj.data('caption') + '</span>'
            );

            $(settings.overlay).fadeIn(function() {
                $(settings.lightbox).show();
            });
        };

        this.each(function (itemIndex, galleryItem) {
            var galleryName = null;
            var galleryItemIndex = itemIndex + 1;

            $(galleryItem).attr('data-index', galleryItemIndex);

            $(galleryItem).on('click', function(e) {
                e.preventDefault();

                if(gallery.length > 1) {
                    $(settings.prev + ',' + settings.next).show();
                }else{
                    $(settings.prev + ',' + settings.next).hide();
                }

                galleryName = $(this).data('gallery');

                render($(this), galleryItemIndex, gallery.length);
            });
        });

        $(settings.overlay + ',' + settings.close).off().on('click', function(e) {
            e.preventDefault();
            $(settings.lightbox).hide(function() {
                $(settings.overlay).fadeOut(100);
            });
        });

        $(settings.prev).off().on('click', function(e) {
            var currentGallery = $(settings.lightbox).attr('data-gallery') * 1;
            var currentIndex = $(settings.lightbox).attr('data-index') * 1;

            var galleryLength = $(settings.item + '[data-gallery="' + currentGallery + '"]').length;

            var nextIndex = currentIndex - 1;
            nextIndex = (nextIndex >= 1) ? nextIndex : galleryLength;

            var nextObj = $(settings.item + '[data-gallery="' + currentGallery + '"][data-index="' + nextIndex + '"]');

            render(nextObj, nextIndex, galleryLength);
        });

        $(settings.next).off().on('click', function(e) {
            var currentGallery = $(settings.lightbox).attr('data-gallery') * 1;
            var currentIndex = $(settings.lightbox).attr('data-index') * 1;

            var galleryLength = $(settings.item + '[data-gallery="' + currentGallery + '"]').length;

            var nextIndex = currentIndex + 1;
            nextIndex = (nextIndex <= galleryLength) ? nextIndex : 1;

            var nextObj = $(settings.item + '[data-gallery="' + currentGallery + '"][data-index="' + nextIndex + '"]');

            render(nextObj, nextIndex, galleryLength);
        });

        return this;

    };

}( jQuery ));